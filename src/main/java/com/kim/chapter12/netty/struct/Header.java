package com.kim.chapter12.netty.struct;

import java.util.HashMap;
import java.util.Map;

public final class Header {

    /**
     * Netty消息的校验码，三部分组成：<br>
     * 1、0xabef：固定值，表示该消息是Netty消息，2个字节<br>
     * 2、主版本号：1-255,1个字节<br>
     * 3、次版本号：1-255,1个字节<br>
     * crcCode=0xabef + 主版本号 + 次版本号<br>
     */
    private int crcCode = 0xabef0101;

    /**
     * 消息长度，整个消息，包括消息头和消息体
     */
    private int length;

    /**
     * 会话ID，集群节点内全局唯一，由会话ID生成器生成
     */
    private long sessionID;

    /**
     * 消息类型
     * 0、业务请求消息
     * 1、业务响应消息
     * 2、业务ONE WAY消息（既是请求，也是响应）
     * 3、握手请求消息
     * 4、握手应发消息
     * 5、心跳请求消息
     * 6、心跳应答消息
     */
    private byte type;

    /**
     * 消息优先级:0-255
     */
    private byte priority;

    /**
     * 可选字段，用于扩展消息头
     */
    private Map<String, Object> attachment = new HashMap<String, Object>(); // 附件

    /**
     * @return the crcCode
     */
    public final int getCrcCode() {
        return crcCode;
    }

    /**
     * @param crcCode
     *            the crcCode to set
     */
    public final void setCrcCode(int crcCode) {
        this.crcCode = crcCode;
    }

    /**
     * @return the length
     */
    public final int getLength() {
        return length;
    }

    /**
     * @param length
     *            the length to set
     */
    public final void setLength(int length) {
        this.length = length;
    }

    /**
     * @return the sessionID
     */
    public final long getSessionID() {
        return sessionID;
    }

    /**
     * @param sessionID
     *            the sessionID to set
     */
    public final void setSessionID(long sessionID) {
        this.sessionID = sessionID;
    }

    /**
     * @return the type
     */
    public final byte getType() {
        return type;
    }

    /**
     * @param type
     *            the type to set
     */
    public final void setType(byte type) {
        this.type = type;
    }

    /**
     * @return the priority
     */
    public final byte getPriority() {
        return priority;
    }

    /**
     * @param priority
     *            the priority to set
     */
    public final void setPriority(byte priority) {
        this.priority = priority;
    }

    /**
     * @return the attachment
     */
    public final Map<String, Object> getAttachment() {
        return attachment;
    }

    /**
     * @param attachment
     *            the attachment to set
     */
    public final void setAttachment(Map<String, Object> attachment) {
        this.attachment = attachment;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Header [crcCode=" + crcCode + ", length=" + length
                + ", sessionID=" + sessionID + ", type=" + type + ", priority="
                + priority + ", attachment=" + attachment + "]";
    }

}
