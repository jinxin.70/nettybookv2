package com.kim.chapter10.http.xml;

import com.kim.chapter10.http.xml.pojo.Address;
import com.kim.chapter10.http.xml.pojo.Customer;
import com.kim.chapter10.http.xml.pojo.Order;
import com.kim.chapter10.http.xml.pojo.Shipping;

import java.util.ArrayList;
import java.util.List;

public class TestOrder {

    public static void main(String[] args) {
        String orderXML = XStreamUtil.beanToXml(getOrder());
        System.out.println(orderXML);

        Order order = XStreamUtil.xmlToBean(orderXML,Order.class);
        System.out.println(order.toString());
//        order.setTotal(3.4f);

    }

    public static Order getOrder(){
        Order order = new Order();
        order.setOrderNumber(1);

        Address address = new Address();
        address.setCity("杭州");
        address.setCountry("中国");
        address.setPostCode("311111");
        address.setState("浙江");
        address.setStreet1("龙翔桥");
        address.setStreet2("黄龙");
        order.setBillTo(address);

        Customer customer = new Customer();
        customer.setCustomerNumber(12);
        customer.setFirstName("K");
        customer.setLastName("B");
        List<String> middleNames = new ArrayList<>();
        middleNames.add("张");
        middleNames.add("孙");
        middleNames.add("赵");
        customer.setMiddleNames(middleNames);
        order.setCustomer(customer);

        order.setShipping(Shipping.INTERNATIONAL_MAIL);

        Address address2 = new Address();
        address2.setCity("乌鲁木齐");
        address2.setCountry("中国");
        address2.setPostCode("311111");
        address2.setState("内蒙古");
        order.setShipTo(address2);

        order.setTotal(2.0f);
        return order;
    }
}
