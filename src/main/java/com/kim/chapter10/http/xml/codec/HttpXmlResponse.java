package com.kim.chapter10.http.xml.codec;

import io.netty.handler.codec.http.FullHttpResponse;

/**
 * 为了和协议栈解耦，同样不直接使用FullHttpResponse，而是封装成HttpXmlResponse
 */
public class HttpXmlResponse {
    private FullHttpResponse httpResponse;
    private Object result;

    /**
     * @param httpResponse
     * @param result 业务需要发送的应答POJO对象
     */
    public HttpXmlResponse(FullHttpResponse httpResponse, Object result) {
        this.httpResponse = httpResponse;
        this.result = result;
    }

    /**
     * @return the httpResponse
     */
    public final FullHttpResponse getHttpResponse() {
        return httpResponse;
    }

    /**
     * @param httpResponse
     *            the httpResponse to set
     */
    public final void setHttpResponse(FullHttpResponse httpResponse) {
        this.httpResponse = httpResponse;
    }

    /**
     * @return the body
     */
    public final Object getResult() {
        return result;
    }

    /**
     * @param result
     *            the body to set
     */
    public final void setResult(Object result) {
        this.result = result;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "HttpXmlResponse [httpResponse=" + httpResponse + ", result="
                + result + "]";
    }

}
