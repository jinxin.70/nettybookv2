package com.kim.chapter10.http.xml.client;

import com.kim.chapter10.http.xml.codec.HttpXmlRequest;
import com.kim.chapter10.http.xml.codec.HttpXmlResponse;
import com.kim.chapter10.http.xml.pojo.OrderFactory;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class HttpXmlClientHandle extends
        SimpleChannelInboundHandler<HttpXmlResponse> {

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        //构造HttpXmlRequest对象
        HttpXmlRequest request = new HttpXmlRequest(null,
                OrderFactory.create(123));
        //发送HttpXmlRequest
        ctx.writeAndFlush(request);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx,
                                   HttpXmlResponse msg) throws Exception {
        //接收已经自动解码后的HttpXmlResponse对象
        System.out.println("The client receive response of http header is : "
                + msg.getHttpResponse().headers().names());
        System.out.println("The client receive response of http body is : "
                + msg.getResult());
    }
}
