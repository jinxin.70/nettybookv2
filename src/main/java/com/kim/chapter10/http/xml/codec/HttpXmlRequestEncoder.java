package com.kim.chapter10.http.xml.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.*;

import java.net.InetAddress;
import java.util.List;

/**
 * 客户端对消息进行编码
 */
public class HttpXmlRequestEncoder extends
        AbstractHttpXmlEncoder<HttpXmlRequest> {

    @Override
    protected void encode(ChannelHandlerContext ctx, HttpXmlRequest msg,
                          List<Object> out) throws Exception {
        //将pojo对象序列化为XML字符串，随后封装为Netty的ByteBuf
        ByteBuf body = encode0(ctx, msg.getBody());
        //对消息头进行判断，如果业务侧自定义和定制了消息头使用业务侧定制的，否则构造新的消息头
        FullHttpRequest request = msg.getRequest();
        if (request == null) {
            //硬编码消息头，一般通信双方更关注消息体，但是如果需要更多灵活性，可以将这些信息配置到配置文件中
            request = new DefaultFullHttpRequest(HttpVersion.HTTP_1_1,
                    HttpMethod.GET, "/do", body);
            HttpHeaders headers = request.headers();
            headers.set(HttpHeaderNames.HOST, InetAddress.getLocalHost()
                    .getHostAddress());
            headers.set(HttpHeaderNames.CONNECTION, HttpHeaderValues.CLOSE);
            headers.set(HttpHeaderNames.ACCEPT_ENCODING,
                    HttpHeaderValues.GZIP.toString() + ','
                            + HttpHeaderValues.DEFLATE.toString());
            headers.set(HttpHeaderNames.ACCEPT_CHARSET,
                    "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
            headers.set(HttpHeaderNames.ACCEPT_LANGUAGE, "zh");
            headers.set(HttpHeaderNames.USER_AGENT,
                    "Netty xml Http Client side");
            headers.set(HttpHeaderNames.ACCEPT,
                    "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        }
        HttpUtil.setContentLength(request, body.readableBytes());
        out.add(request);
    }

}