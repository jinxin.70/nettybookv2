package com.kim.chapter10.http.xml.client;

import com.kim.chapter10.http.xml.codec.HttpXmlRequestEncoder;
import com.kim.chapter10.http.xml.codec.HttpXmlResponseDecoder;
import com.kim.chapter10.http.xml.pojo.Order;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestEncoder;
import io.netty.handler.codec.http.HttpResponseDecoder;

import java.net.InetSocketAddress;

public class HttpXmlClient {

    public void connect(int port) throws Exception {
        // 配置客户端NIO线程组
        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group).channel(NioSocketChannel.class)
                    .option(ChannelOption.TCP_NODELAY, true)
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch)
                                throws Exception {

                            //负责将二进制码流解码成HTTP应答消息
                            ch.pipeline().addLast("http-decoder",
                                    new HttpResponseDecoder());
                            //将一个HTTP请求的多个部分合并成一个完成的HTTP消息
                            ch.pipeline().addLast("http-aggregator",
                                    new HttpObjectAggregator(65536));
                            // XML响应解码器，解码服务端的响应消息
                            ch.pipeline().addLast(
                                    "xml-decoder",
                                    new HttpXmlResponseDecoder(Order.class,
                                            true));

                            //编码的时候按照从后往前的顺序调度进行
                            ch.pipeline().addLast("http-encoder",
                                    new HttpRequestEncoder());
                            // XML请求编码器，将客户端消息编码
                            ch.pipeline().addLast("xml-encoder",
                                    new HttpXmlRequestEncoder());
                            //添加业务逻辑编排类
                            ch.pipeline().addLast("xmlClientHandler",
                                    new HttpXmlClientHandle());
                        }
                    });

            // 发起异步连接操作
            ChannelFuture f = b.connect(new InetSocketAddress(port)).sync();

            // 当客户端链路关闭
            f.channel().closeFuture().sync();
        } finally {
            // 优雅退出，释放NIO线程组
            group.shutdownGracefully();
        }
    }

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        int port = 8080;
        if (args != null && args.length > 0) {
            try {
                port = Integer.valueOf(args[0]);
            } catch (NumberFormatException e) {
                // 采用默认值
            }
        }
        new HttpXmlClient().connect(port);
    }
}
